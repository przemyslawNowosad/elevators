import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);
const BASE_URL = 'http://localhost:8888/rest/v1/';

export default new Vuex.Store({
    state: {
        elevators: [],
        selectedElevator: {},
        elevatorConfig: {
            numberOfFloors: 0,
            numberOfElevators: 0,
            travelTime: 0
        },
        getElevatorsUrl: `${BASE_URL}elevators/`,
        getConfigUrl: `${BASE_URL}config/`,
        requestElevatorUrl: `${BASE_URL}request/`,
        releaseElevatorUrl: `${BASE_URL}release/`,
        sseUrl: `${BASE_URL}sse`
    },
    mutations: {
        refreshElevators(state, fetched) {
            state.elevators = fetched;
            let elevator = fetched.filter((elevator) => elevator.id === state.selectedElevator.id)[0];
            if (!(elevator === undefined)) {
                state.selectedElevator = elevator;
            }
        },
        loadConfig(state, fetched) {
            state.elevatorConfig = fetched;
        },
        pickElevator(state, elevator) {
            state.selectedElevator = elevator;
        }
    },
    actions: {
        getElevators({commit}) {
            axios.get(this.state.getElevatorsUrl).then((response) => {
                commit('refreshElevators', response.data);
            }, () => console.log('Failed to refresh elevators'));
        },
        getConfig({commit}) {
            axios.get(this.state.getConfigUrl).then((response) => {
                commit('loadConfig', response.data);
            }, () => console.log('Failed to load config'));
        },
        setSelectedElevator({commit}, elevator) {
            commit('pickElevator', elevator);
        },
        requestElevator({commit}, floor) {
            axios.get(`${this.state.requestElevatorUrl}${floor}`).then((response) => console.log(response));
        },
        releaseElevator({commit}, elevatorId) {
            axios.get(`${this.state.releaseElevatorUrl}${elevatorId}`).then((response) => console.log(response));
        },
        sseSetup({commit}) {
            let es = new EventSource(this.state.sseUrl);
            es.addEventListener('message', ev => {
                commit('refreshElevators', JSON.parse(ev.data));
            })
        }
    }
});
