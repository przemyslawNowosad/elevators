package com.tingco.codechallenge.elevator;

import com.jayway.awaitility.Duration;
import com.tingco.codechallenge.elevator.app.Elevator;
import com.tingco.codechallenge.elevator.app.ElevatorController;
import com.tingco.codechallenge.elevator.domain.ElevatorControllerImpl;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Random;

import static com.jayway.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Boiler plate test class to get up and running with a test faster.
 *
 * @author Sven Wesley
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ElevatorApplication.class)
public class IntegrationTest {

    @Value("${com.tingco.elevator.numberofelevators}")
    private int numberOfElevators;

    @Value("${com.tingco.elevator.numberoffloors}")
    private int numberOfFloors;

    @Autowired
    private ElevatorController elevatorController;

    @After
    public void cleanUp() {
        elevatorController.resetAll();
    }

    @Test
    public void simulateElevatorShaftBasicScenario() {
        // when + then
        Elevator elevator = elevatorController.requestElevator(7).get();
        await().timeout(Duration.ONE_SECOND).until(() -> !elevator.isNotBusy());
        await().timeout(Duration.ONE_MINUTE).until(elevator::isNotBusy);
        assertTrue(elevator.isNotBusy());
        assertThat(elevator.getCurrentFloor(), is(7));
    }

    @Test
    public void shouldUseAllAvailableElevatorsAndQueueRequestOverAvailability() {
        LinkedList<Optional<Elevator>> elevators = new LinkedList<>();
        for (int i = 0; i <= numberOfElevators; i++) {
            elevators.push(elevatorController.requestElevator(numberOfFloors - 1));
        }
        assertThat(elevators.size(), is(numberOfElevators + 1));
        assertFalse(elevators.pop().isPresent());
    }

    @Test
    public void shouldNearestElevatorRespondToCall() {

        Optional<Elevator> elevator1 = elevatorController.requestElevator(11);
        Optional<Elevator> elevator2 = elevatorController.requestElevator(10);
        Optional<Elevator> elevator3 = elevatorController.requestElevator(9);
        Optional<Elevator> elevator4 = elevatorController.requestElevator(3);
        await().timeout(Duration.ONE_MINUTE).until(() -> elevatorController.getElevators().stream().filter(Elevator::isNotBusy).count() == 4);
        Optional<Elevator> elevatorLastRequest = elevatorController.requestElevator(5);
        assertEquals(elevator4.get().getId(), elevatorLastRequest.get().getId());
    }

    @Test
    public void shouldAllRequestsBeFulfilled() {
        elevatorController.requestElevator(11);
        while (((ElevatorControllerImpl) elevatorController).getRequests().size() < 5) {
            elevatorController.requestElevator(new Random().nextInt(numberOfFloors));
        }
        await().atMost(Duration.TWO_MINUTES).until(() -> (((ElevatorControllerImpl) elevatorController).getRequests().isEmpty()));
    }

}
