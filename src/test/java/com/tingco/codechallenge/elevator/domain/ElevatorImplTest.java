package com.tingco.codechallenge.elevator.domain;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.elevator.app.Elevator;
import com.tingco.codechallenge.elevator.exceptions.FloorOutOfRangeException;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class ElevatorImplTest {

    private int topFloor = 7;

    private EventBus eventBus = mock(EventBus.class);

    private ElevatorImpl elevator = new ElevatorImpl(topFloor, eventBus);

    @Test
    public void shouldBeValidElevatorOnCreation() {
        // then
        assertTrue(elevator.isNotBusy());
        assertThat(elevator.getCurrentFloor(), is(0));
        assertThat(elevator.getAddressedFloor(), is(0));
        assertThat(elevator.getDirection(), is(Elevator.Direction.NONE));
        assertThat(elevator.getTopFloor(), is(topFloor));
    }

    @Test
    public void shouldAssignTargetFloorToGivenFloor() {
        // given
        int requestedFloor = 7;
        // when
        elevator.moveElevator(requestedFloor);
        // then
        int addressedFloor = elevator.getAddressedFloor();
        assertThat(addressedFloor, is(requestedFloor));
        assertEquals(addressedFloor, elevator.getAddressedFloor());
    }

    @Test(expected = FloorOutOfRangeException.class)
    public void shouldNotMoveToFloorOutsideRange() {
        // given
        int requestedFloor = topFloor + 1;
        // when + then
        elevator.moveElevator(requestedFloor);
    }

}