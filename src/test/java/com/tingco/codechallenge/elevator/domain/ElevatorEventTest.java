package com.tingco.codechallenge.elevator.domain;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.elevator.app.Elevator;
import org.junit.Test;
import org.mockito.Mock;

import java.util.UUID;

import static com.tingco.codechallenge.elevator.domain.ElevatorEvent.Type.REQUEST_PENDING;
import static com.tingco.codechallenge.elevator.domain.ElevatorEvent.Type.RIDE_STARTED;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ElevatorEventTest {

    @Mock
    private EventBus eventBus = mock(EventBus.class);

    @Test
    public void shouldCreateEvent() {
        // given
        Elevator elevator = basicElevator();
        // when
        ElevatorEvent elevatorEvent = new ElevatorEvent(elevator, RIDE_STARTED);
        // then
        assertThat(elevatorEvent.getElevator().getId(), is(elevator.getId()));
        assertThat(elevatorEvent.getElevatorEventType(), is(RIDE_STARTED));
    }

    @Test
    public void shouldFailCreateEventDueToNullEvent() {
        // given
        UUID elevatorId = UUID.randomUUID();
        // when + then
        assertThatThrownBy(() -> new ElevatorEvent(basicElevator(), null))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("elevator must not be null");
    }

    @Test
    public void shouldFailCreateEventDueToPendingEventTypeAndElevatorAssigned() {
        // when + then
        assertThatThrownBy(() -> new ElevatorEvent(basicElevator(), REQUEST_PENDING))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("pending request cannot have elevator assigned");
    }

    private Elevator basicElevator() {
        return new ElevatorImpl(7, eventBus);
    }

}