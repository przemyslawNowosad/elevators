package com.tingco.codechallenge.elevator.domain;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.elevator.app.Elevator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ElevatorControllerImplTest {

    private static final int TOP_FLOOR = 7;

    @Mock
    private EventBus eventBus;

    @Captor
    private ArgumentCaptor<ElevatorEvent> eventCaptor;

    @InjectMocks
    private ElevatorControllerImpl elevatorController;

    @Test
    public void shouldReleaseElevator() {
        // given
        Elevator elevator = new ElevatorImpl(TOP_FLOOR, eventBus);
        elevatorController.addElevators(Collections.singleton(elevator));
        // when
        elevatorController.releaseElevator(elevator);
        // then
        assertEquals(Elevator.Direction.NONE, elevator.getDirection());
        assertEquals(elevator.getCurrentFloor(), elevator.getAddressedFloor());
        assertTrue(elevator.isNotBusy());
        verify(eventBus, times(1)).post(eventCaptor.capture());
        ElevatorEvent capturedEvent = eventCaptor.getValue();
        assertThat(capturedEvent.getElevatorEventType(), is(ElevatorEvent.Type.ELEVATOR_RELEASED));
        assertThat(capturedEvent.getElevator(), is(elevator));
    }

    @Test
    public void shouldReturnCompleteListOfElevators() {
        // given
        int numOfElevators = 5;
        Set<Elevator> elevators = prepareElevators(numOfElevators, 0);
        // when
        List<Elevator> elevatorList = elevatorController.getElevators();
        // then
        assertNotNull(elevatorList);
        assertThat(elevatorList.size(), is(numOfElevators));
        assertTrue(elevatorList.containsAll(elevators));
    }

    @Test
    public void shouldRequestElevator() {
        // given
        elevatorController.addElevators(Collections.singleton(new ElevatorImpl(TOP_FLOOR, eventBus)));
        // when
        elevatorController.requestElevator(6);
        // then
        Elevator elevator = elevatorController.getElevators().get(0);
        assertFalse(elevator.isNotBusy());
        assertThat(elevator.getCurrentFloor(), is(0));
        assertThat(elevator.getAddressedFloor(), is(6));
        assertThat(elevator.getDirection(), is(Elevator.Direction.UP));
    }


    @Test
    public void shouldRequestsBeQueuedWhenAllElevatorsBusy() {
        // given
        elevatorController.addElevators(prepareElevators(5, 0));
        elevatorController.requestElevator(7);
        elevatorController.requestElevator(7);
        elevatorController.requestElevator(7);
        elevatorController.requestElevator(7);
        elevatorController.requestElevator(7);
        // when
        elevatorController.requestElevator(4);
        elevatorController.requestElevator(5);
        // then
        assertTrue(elevatorController.getElevators().stream().noneMatch(Elevator::isNotBusy));
        Queue<Integer> requests = elevatorController.getRequests();
        assertThat(requests, not(empty()));
        assertThat(requests.poll(), is(4));
        assertThat(requests.peek(), is(5));
    }

    @Test
    public void shouldPickOnlyIdle() {
        // given
        Set<Elevator> elevators = prepareElevators(3, 0);
        elevatorController.addElevators(elevators);
        elevatorController.requestElevator(5);
        elevatorController.requestElevator(6);
        UUID elevatorId = elevatorController.getElevators().stream().filter(Elevator::isNotBusy).findAny().map(Elevator::getId).get();
        // when
        Elevator chosenElevator = elevatorController.requestElevator(4).get();
        // then
        assertThat(chosenElevator.getId(), is(elevatorId));
    }

    @Test
    public void shouldPickNearestIdleElevator() {
        // given
        Set<Elevator> elevators = new HashSet<>();
        ElevatorImpl e1 = new ElevatorImpl(TOP_FLOOR, eventBus);
        e1.setCurrentFloor(6);
        e1.setDirection(Elevator.Direction.UP);
        elevators.add(e1);
        ElevatorImpl e2 = new ElevatorImpl(TOP_FLOOR, eventBus);
        e2.setCurrentFloor(5);
        elevators.add(e2);
        ElevatorImpl e3 = new ElevatorImpl(TOP_FLOOR, eventBus);
        e3.setCurrentFloor(4);
        elevators.add(e3);
        elevatorController.addElevators(elevators);
        // when
        Elevator chosenElevator = elevatorController.requestElevator(7).get();
        // then
        assertThat(chosenElevator, is(e2));
    }

    private Set<Elevator> prepareElevators(int numOfElevators, int startingFloor) {
        Set<Elevator> elevators = new HashSet<>();
        for (int i = 0; i < numOfElevators; i++) {
            ElevatorImpl elevator = new ElevatorImpl(TOP_FLOOR, eventBus);
            elevator.setCurrentFloor(startingFloor);
            elevators.add(elevator);
        }
        elevatorController.addElevators(elevators);
        return elevators;
    }
}