package com.tingco.codechallenge.elevator.api;

import com.tingco.codechallenge.elevator.app.Elevator;
import com.tingco.codechallenge.elevator.ElevatorApplication;
import com.tingco.codechallenge.elevator.domain.ElevatorControllerImpl;
import com.tingco.codechallenge.elevator.exceptions.ElevatorNotFoundException;
import com.tingco.codechallenge.elevator.exceptions.FloorOutOfRangeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openapitools.model.Elevators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Boiler plate test class to get up and running with a test faster.
 *
 * @author Sven Wesley
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElevatorApplication.class)
public class ElevatorControllerEndPointsIntegrationTest {

    @Value("${com.tingco.elevator.numberoffloors}")
    private int numberOfFloors;

    @Autowired
    private ElevatorControllerImpl elevatorController;

    @Autowired
    private ElevatorControllerEndPoints endPoints;

    @Test
    public void shouldPing() {
        ResponseEntity<String> response = endPoints.ping();
        assertEquals("pong", response.getBody());
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void shouldGetAllElevators() {
        ResponseEntity<Elevators> response = endPoints.getElevators();
        assertEquals(response.getBody().stream()
                        .map(org.openapitools.model.Elevator::getId)
                        .collect(Collectors.toList()),
                elevatorController.getElevators().stream()
                        .map(Elevator::getId)
                        .collect(Collectors.toList()));
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void shouldGetSpecificElevator() {
        // given
        List<UUID> elevators = endPoints.getElevators().getBody().stream().map(org.openapitools.model.Elevator::getId).collect(Collectors.toList());
        List<Elevator> elevatorsFromService = elevatorController.getElevators();
        // when + then
        elevators.stream().map(e -> endPoints.getElevator(e))
                .forEach(elevator -> {
                    assertTrue(elevators.contains(elevator.getBody().getId()));
                    assertThat(elevator.getStatusCode(), is(HttpStatus.OK));
                });
        List<UUID> elevatorsIds = elevatorsFromService.stream()
                .map(Elevator::getId)
                .collect(Collectors.toList());
        assertEquals(elevatorsIds,
                elevatorsIds.stream()
                        .map(endPoints::getElevator)
                        .map(HttpEntity::getBody)
                        .map(org.openapitools.model.Elevator::getId)
                        .collect(Collectors.toList()));
    }

    @Test
    public void shouldFailGetSpecificElevatorWhenNotExistant() {
        // given
        UUID nonExistingElevatorId = UUID.randomUUID();
        // when + them
        assertThatThrownBy(() -> {
            endPoints.getElevator(nonExistingElevatorId);
        })
                .isInstanceOf(ElevatorNotFoundException.class)
                .hasMessage(String.format(ElevatorNotFoundException.MESSAGE, nonExistingElevatorId));
    }

    @Test
    public void shouldRequestElevatorToFloor() {
        // given
        Integer requestedFloor = numberOfFloors - 1;
        // when
        ResponseEntity<org.openapitools.model.Elevator> response = endPoints.requestElevator(requestedFloor);
        // then
        assertTrue(response.hasBody());
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        org.openapitools.model.Elevator elevator = response.getBody();
        assertEquals(requestedFloor, elevator.getAddressedFloor());
    }

    @Test
    public void shouldFailRequestElevatorToFloorOutOfRange() {
        // given
        Integer requestedFloor = numberOfFloors + 1;
        // when + then
        assertThatThrownBy(() -> endPoints.requestElevator(requestedFloor))
                .isInstanceOf(FloorOutOfRangeException.class)
                .hasMessage(String.format(FloorOutOfRangeException.MESSAGE, requestedFloor));
    }

    @Test
    public void shouldReleaseElevatorFromDuty() {
        // given
        Integer requestedFloor = numberOfFloors - 1;
        // when
        ResponseEntity<org.openapitools.model.Elevator> requestResponse = endPoints.requestElevator(requestedFloor);
        ResponseEntity<org.openapitools.model.Elevator> releaseResponse = endPoints.releaseElevator(requestResponse.getBody().getId());
        // then
        assertTrue(requestResponse.hasBody());
        org.openapitools.model.Elevator elevator = requestResponse.getBody();
        assertEquals(requestedFloor, elevator.getAddressedFloor());
        assertEquals(requestResponse.getBody().getId(), releaseResponse.getBody().getId());
        assertThat(releaseResponse.getStatusCode(), is(HttpStatus.OK));
        assertThat(requestResponse.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void shouldFailReleaseNoSuchElevator() {
        // given
        UUID nonExistingElevatorId = UUID.randomUUID();
        // when + them
        assertThatThrownBy(() -> {
            endPoints.releaseElevator(nonExistingElevatorId);
        })
                .isInstanceOf(ElevatorNotFoundException.class)
                .hasMessage(String.format(ElevatorNotFoundException.MESSAGE, nonExistingElevatorId));
    }

}
