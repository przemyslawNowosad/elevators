package com.tingco.codechallenge.elevator.app;

import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;


/**
 * Interface for the Elevator Controller.
 *
 * @author Sven Wesley
 */
public interface ElevatorController {

    /**
     * Request an elevator to the specified floor.
     *
     * @param toFloor addressed floor as integer.
     * @return The Elevator that is going to the floor, if there is one to move.
     */
    Optional<Elevator> requestElevator(int toFloor);

    /**
     * A snapshot list of all elevators in the system.
     *
     * @return A List with all {@link Elevator} objects.
     */
    List<Elevator> getElevators();

    /**
     * A specified elevator
     *
     * @param elevatorId requested elevator id
     * @return A {@link Elevator} object with given id.
     */
    Optional<Elevator> getElevator(UUID elevatorId);

    /**
     * Telling the controller that the given elevator is free for new
     * operations.
     *
     * @param elevator the elevator that shall be released.
     */
    Elevator releaseElevator(Elevator elevator);

    /**
     * Method to add elevators to existing ElevatorController
     *
     * @param elevators the elevators that shall be added.
     */
    boolean addElevators(Set<Elevator> elevators);

    /**
     * Service feature to restore initial state of elevator shaft
     */
    void resetAll();

    /**
     * Endpoint for Server Sent Events. Target is to replace frontend polling.
     * @return Flux of elevators to update state of API Consumer
     */
    Flux<Set<Elevator>> sse();
}
