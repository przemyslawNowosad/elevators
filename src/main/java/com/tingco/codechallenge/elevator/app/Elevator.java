package com.tingco.codechallenge.elevator.app;

import java.util.UUID;

/**
 * Interface for an elevator object.
 *
 * @author Sven Wesley
 *
 */
public interface Elevator extends Runnable {

    /**
     * Enumeration for describing elevator's direction.
     */
    public enum Direction {
        UP, DOWN, NONE;

        public String asValue() {
            return this.toString().toLowerCase();
        }
    }

    /**
     * Tells which direction is the elevator going in.
     *
     * @return Direction Enumeration value describing the direction.
     */
    Direction getDirection();

    /**
     * If the elevator is moving. This is the target floor.
     *
     * @return primitive integer number of floor
     */
    int getAddressedFloor();

    /**
     * Get the Id of this elevator.
     *
     * @return UUID representing the elevator.
     */
    UUID getId();

    /**
     * Command to move the elevator to the given floor.
     *
     * @param toFloor int where to go.
     */
    void moveElevator(int toFloor);

    /**
     * Check if the elevator is occupied at the moment.
     *
     * @return true if not busy.
     */
    boolean isNotBusy();

    /**
     * Reports which floor the elevator is at right now.
     *
     * @return int actual floor at the moment.
     */
    int getCurrentFloor();

    /**
     * Allows to release elevator
     *
     * @return Elevator that was released.
     */
    Elevator release();

    /**
     * Computes distance between current floor and param
     *
     * @param floor to compute for
     * @return int distance between floors
     */
    int getDistanceTo(int floor);

}
