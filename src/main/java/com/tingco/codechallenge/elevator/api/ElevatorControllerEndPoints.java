package com.tingco.codechallenge.elevator.api;

import com.tingco.codechallenge.elevator.app.Elevator;
import com.tingco.codechallenge.elevator.domain.ElevatorControllerImpl;
import com.tingco.codechallenge.elevator.exceptions.ElevatorNotFoundException;
import org.openapitools.api.ElevatorControllerEndPointsApi;
import org.openapitools.model.Config;
import org.openapitools.model.Elevators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Rest Resource.
 *
 * @author Sven Wesley
 */
@CrossOrigin
@RestController
@RequestMapping("/rest/v1")
public class ElevatorControllerEndPoints implements ElevatorControllerEndPointsApi {

    @Autowired
    private final ElevatorControllerImpl elevatorController;

    public ElevatorControllerEndPoints(ElevatorControllerImpl elevatorController) {
        this.elevatorController = elevatorController;
    }

    /**
     * Ping service to test if we are alive.
     *
     * @return String pong
     */
    @Override
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok("pong");
    }

    /**
     * Get all elevators meaningful data
     *
     * @return list of all elevator object api representations
     */
    @Override
    public ResponseEntity<Elevators> getElevators() {
        return ResponseEntity.ok(mapToResponseObject(elevatorController.getElevators()));
    }

    @Override
    public ResponseEntity<Config> getConfig() {
        Config config = new Config();
        config.setNumberOfElevators(elevatorController.getNumberOfElevators());
        config.setNumberOfFloors(elevatorController.getNumberOfFloors());
        config.setTravelTime(elevatorController.getDelayBeforeReachingFloor());
        return ResponseEntity.ok(config);
    }

    /**
     * Get specific elevator
     *
     * @param elevatorId uuid of queried elevator
     * @return elevator object api representation
     */
    @Override
    public ResponseEntity<org.openapitools.model.Elevator> getElevator(UUID elevatorId) {
        com.tingco.codechallenge.elevator.app.Elevator elevator
                = elevatorController.getElevator(elevatorId).orElseThrow(() -> new ElevatorNotFoundException(elevatorId));
        return ResponseEntity.ok(mapToResponseObject(elevator));
    }

    /**
     * Release elevator from current task. Elevator stops on current floor
     *
     * @param elevatorId uuid of queried elevator
     * @return elevator object api representation
     */
    @Override
    public ResponseEntity<org.openapitools.model.Elevator> releaseElevator(UUID elevatorId) {
        Elevator elevator = elevatorController.getElevator(elevatorId).orElseThrow(() -> new ElevatorNotFoundException(elevatorId));
        return ResponseEntity.ok(mapToResponseObject(elevatorController.releaseElevator(elevator)));
    }

    /**
     * Release elevator from current task. Elevator stops on current floor
     *
     * @param floorNumber floor requested to serve by elevator
     * @return elevator object api representation. returns null if request is pending in queue
     * asynchronous to be implemented
     */
    @Override
    public ResponseEntity<org.openapitools.model.Elevator> requestElevator(Integer floorNumber) {
        // TODO handle async until elevator assigned to request
        return ResponseEntity.ok(elevatorController.requestElevator(floorNumber).map(this::mapToResponseObject).orElse(null));
    }

    @GetMapping(path = "/sse", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Elevators> sse() {
        return elevatorController.sse().map(this::mapToResponseObject);
    }

    private Elevators mapToResponseObject(Collection<Elevator> elevators) {
        Elevators elevatorsObject = new Elevators();
        elevatorsObject.addAll(elevators.stream().map(this::mapToResponseObject).collect(Collectors.toList()));
        return elevatorsObject;
    }

    private org.openapitools.model.Elevator mapToResponseObject(com.tingco.codechallenge.elevator.app.Elevator elevator) {
        org.openapitools.model.Elevator mappedElevator = new org.openapitools.model.Elevator();
        mappedElevator.setId(elevator.getId());
        mappedElevator.setCurrentFloor(elevator.getCurrentFloor());
        mappedElevator.setAddressedFloor(elevator.getAddressedFloor());
        mappedElevator.setDirection(org.openapitools.model.Elevator.DirectionEnum.fromValue(elevator.getDirection().asValue()));
        return mappedElevator;
    }

}
