package com.tingco.codechallenge.elevator.domain;

import com.tingco.codechallenge.elevator.app.Elevator;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

@Getter
public class ElevatorEvent {

    @NotNull
    private final UUID id;
    private final Elevator elevator;
    @NotNull
    private final ElevatorEvent.Type elevatorEventType;

    public ElevatorEvent(Elevator elevator, ElevatorEvent.Type elevatorEventType) {
        Objects.requireNonNull(elevatorEventType, "elevator must not be null");
        if (elevator != null && elevatorEventType == Type.REQUEST_PENDING) {
            throw new IllegalArgumentException("pending request cannot have elevator assigned");
        }
        this.id = UUID.randomUUID();
        this.elevator = elevator;
        this.elevatorEventType = elevatorEventType;
    }

    public Type getElevatorEventType() {
        return elevatorEventType;
    }

    public enum Type {
        REQUEST_PENDING,
        RIDE_STARTED,
        RIDE_COMPLETED,
        ELEVATOR_RELEASED
    }
}
