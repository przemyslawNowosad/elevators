package com.tingco.codechallenge.elevator.domain;

import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.elevator.app.Elevator;
import com.tingco.codechallenge.elevator.exceptions.FloorOutOfRangeException;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
public class ElevatorImpl implements Elevator, Runnable {

    private final UUID id;
    private final int topFloor;
    private final EventBus eventBus;
    private int currentFloor;
    private int addressedFloor;
    private Elevator.Direction direction;

    public ElevatorImpl(int topFloor, EventBus eventBus) {
        Objects.requireNonNull(eventBus);
        this.id = UUID.randomUUID();
        this.currentFloor = 0;
        this.addressedFloor = 0;
        this.direction = Direction.NONE;
        this.topFloor = topFloor;
        this.eventBus = eventBus;
    }

    @Override
    public void run() {
        eventBus.post(new ElevatorEvent(this, ElevatorEvent.Type.RIDE_STARTED));
        Direction currentDirection = getDirection();
        if (currentDirection == Direction.UP) {
            currentFloor++;
        } else if (currentDirection == Direction.DOWN) {
            currentFloor--;
        }
        if (Direction.NONE != direction && currentFloor == addressedFloor) {
            setDirection(Direction.NONE);
            eventBus.post(new ElevatorEvent(this, ElevatorEvent.Type.RIDE_COMPLETED));
        }
    }

    @Override
    public void moveElevator(int toFloor) {
        validateRequestedFloor(toFloor);
        if (toFloor > currentFloor) {
            setDirection(Direction.UP);
        } else if (toFloor < currentFloor) {
            setDirection(Direction.DOWN);
        } else {
            return;
        }
        addressedFloor = toFloor;
    }

    @Override
    public boolean isNotBusy() {
        return Direction.NONE == direction;
    }

    @Override
    public Elevator release() {
        addressedFloor = currentFloor;
        setDirection(Direction.NONE);
        eventBus.post(new ElevatorEvent(this, ElevatorEvent.Type.ELEVATOR_RELEASED));
        return this;
    }

    public int getDistanceTo(int floor) {
        return Math.abs(currentFloor - floor);
    }

    private void validateRequestedFloor(int targetFloor) {
        if (targetFloor < 0 || targetFloor > topFloor) {
            throw new FloorOutOfRangeException(targetFloor);
        }
    }

}
