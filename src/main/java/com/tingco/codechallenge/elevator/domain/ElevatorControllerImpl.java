package com.tingco.codechallenge.elevator.domain;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.tingco.codechallenge.elevator.app.Elevator;
import com.tingco.codechallenge.elevator.app.ElevatorController;
import lombok.Getter;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class ElevatorControllerImpl implements ElevatorController {

    @Value("${com.tingco.elevator.numberofelevators}")
    @Getter
    private int numberOfElevators;
    @Value("${com.tingco.elevator.numberoffloors}")
    @Getter
    private int numberOfFloors;
    @Value("${com.tingco.elevator.delayBetweenReachingFloor}")
    @Getter
    private int delayBeforeReachingFloor;

    private final Set<Elevator> elevators = new HashSet<>(numberOfElevators);
    private Queue<Integer> requests = new LinkedList<>();

    private final ScheduledExecutorService executorService;
    private final EventBus eventBus;

    @Autowired
    public ElevatorControllerImpl(ScheduledExecutorService executorService, EventBus eventBus) {
        this.executorService = executorService;
        this.eventBus = eventBus;
    }

    @PostConstruct
    public void prepareController() {
        elevatorsSetup();
        eventBus.register(this);
    }

    @Override
    @Synchronized
    public Optional<Elevator> requestElevator(int toFloor) {
        Optional<Elevator> chosenElevator = elevators.stream()
                .filter(Elevator::isNotBusy)
                .min(Comparator.comparing(elevator -> elevator.getDistanceTo(toFloor)));
        chosenElevator.ifPresent(elevator -> {
            elevator.moveElevator(toFloor);
        });
        if (!chosenElevator.isPresent()) { // TODO java 9 makes it a lot nicer with ifPresentOrElse, consider migration
            queueRequest(toFloor);
            eventBus.post(new ElevatorEvent(null, ElevatorEvent.Type.REQUEST_PENDING));
        }
        return chosenElevator;
    }

    @Subscribe
    public void handleEvent(ElevatorEvent event) {
        if (ElevatorEvent.Type.RIDE_COMPLETED.equals(event.getElevatorEventType())) {
            releaseElevator(event.getElevator());
        }
        if (ElevatorEvent.Type.ELEVATOR_RELEASED.equals(event.getElevatorEventType()) && !requests.isEmpty()) {
            requestElevator(requests.poll());
        }
    }

    @Override
    @Synchronized
    public Elevator releaseElevator(Elevator elevator) {
        return elevator.release();
    }

    @Override
    public List<Elevator> getElevators() {
        return new ArrayList<>(elevators);
    }

    @Override
    public Optional<Elevator> getElevator(UUID elevatorId) {
        return getElevators().stream().filter(elevator -> elevator.getId().equals(elevatorId)).findFirst();
    }

    @Override
    public boolean addElevators(Set<Elevator> elevators) {
        return this.elevators.addAll(elevators);
    }

    @Override
    public void resetAll() {
        elevatorsSetup();
        requests.clear();
    }

    @Override
    public Flux<Set<Elevator>> sse() {
        // TODO make reactive - publish only when elevator state changes
        return Flux.interval(Duration.ofSeconds(1)).map(second -> elevators);
    }

    /**
     * for debugging purposes
     *
     * @return list of currently pending requests
     */
    public Queue<Integer> getRequests() {
        return requests;
    }

    private boolean queueRequest(int toFloor) {
        return requests.add(toFloor);
    }

    private void elevatorsSetup() {
        elevators.clear();
        for (int i = 0; i < numberOfElevators; i++) {
            elevators.add(new ElevatorImpl(numberOfFloors - 1, eventBus));
        }
        elevators.forEach(elevator -> executorService.scheduleAtFixedRate(elevator, 500, delayBeforeReachingFloor, TimeUnit.MILLISECONDS));
    }
}
