package com.tingco.codechallenge.elevator.exceptions;

import java.util.UUID;

public class ElevatorNotFoundException extends RuntimeException {

    public static final String MESSAGE = "Elevator with id %s was not found";

    public ElevatorNotFoundException(UUID id) {
        super(String.format("Elevator with id %s was not found", id));
    }
}
