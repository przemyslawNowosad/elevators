package com.tingco.codechallenge.elevator.exceptions;

public class FloorOutOfRangeException extends RuntimeException {

    public static final String MESSAGE = "Floor %s out of floor range";

    public FloorOutOfRangeException(int floor) {
        super(String.format(MESSAGE, floor));
    }
}
