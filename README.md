# elevators

## Tingco Codechallenge

Thank you for reviewing my implementation of the given challenge. I had fun doing it and I really wish I had more time to work on it. However - I hope you like what you see and we can talk it through.

All the meaningful params can be edited in application.properties file.

*com.tingco.elevator.numberofelevators* indicates number of elevators. this can be any positive integer up to what int allows. default is **4**

*com.tingco.elevator.numberoffloors* indicates number of floors. note that counting starts from 0, so you can request elevator to any floor in range 0..numberoffloors - 1. default is **33**

*com.tingco.elevator.delayBetweenReachingFloor* inicates how much time elevator needs to reach next floor in milliseconds. I recommend to keep this value low for integration tests with awaitility and a bit higher during manual tests to be able to e.g. release an elevator during ride (cancel ride in other words). default is **2000**

*server.port* port you would like to run app on. default is **8888**

## Testing application 
One way is to run unit and integration tests. IntegrationTest.class consists of some scenarios that test correct behavior of the whole system.

Second - testing in swagger UI:

1. Run backend application.
2. Go to ```<server>:<port>/swagger-ui.html``` (default:    ```localhost:8888/swagger-ui.html``` ).
3. Play with different endpoints. E.g. you can request the list of elevators and check how their parameters change when you request/release.

Third - most complete, to test in provided UI:

1. Run backend application. Currently it has to run on hardcoded port :8888
2. option a: in directory ```<project dir>/elevators/elevators``` run ```yarn serve``` and open ```localhost:8080``` in the browser
2. option b: in directory ```<project dir>/elevators/elevators``` run ```yarn run build``` and open generated index.html
3. hope you will have fun playing with application. it was tested on Google Chrome and Mozilla Firefox

In case of any questions - please do not hesitate to contact me.

## TODOs

This is a list of steps I would perform next when carry on working on the project.

1. extend requesting elevator with Direction indication
2. based on floor and direction: in controller check if:
	1. requested floor is on the way of any elevator
	2. direction matches
	3. (then sort by closest)
	4. request elevator to stop/open doors on requested floor without calling new elevator or adding pending request
3. logging is very basic - extend by:
 	1. measure execution time for timely operations
 	2. add some visualisation feature for easier problem tracing like Kibana
4. get rid of dependency to springfox - with OpenApi3 it is no longer necessary and allows more fine grained control over swagger ui
5. add security to api
6. consider fetching Elevator async in rest endpoint when request is queued
7. send SSE only when state changes instead interval
8. frontend tests